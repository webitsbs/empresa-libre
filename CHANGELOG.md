v 1.6.1 [09-abr-2018]
---------------------
    - Fix: Nómina con separación


v 1.6.0 [18-feb-2018]
---------------------
    - Facturacion a extranjeros

* IMPORTANTE: Es necesario realizar una migración, despues de actualizar la rama principal.

```
git pull origin master

cd source/app/models

python main.py -m
```

v 1.5.0 [30-ene-2018]
---------------------
    - Timbrado de Nómina

* IMPORTANTE: Es necesario realizar una migración, despues de actualizar la rama principal.

```
git pull origin master

cd source/app/models

python main.py -m
```

v 1.4.0 [01-ene-2018]
---------------------
    - Impresión de tickets

v 1.3.0 [27-Dic-2017]
---------------------
    - Punto de venta

* IMPORTANTE: Es necesario realizar una migración, despues de actualizar la rama principal.

```
git pull origin master

cd source/app/models

python main.py -m
```

v 1.2.0 [18-Dic-2017]
---------------------
* IMPORTANTE: Es necesario actualizar la base de datos, despues de actualizar la rama principal.

```
git pull origin master

cd source/app/models

python main.py -bd
```

v 0.1.0 [26-Oct-2017]
---------------------
    - Generar y timbrar con CFDI 3.3

