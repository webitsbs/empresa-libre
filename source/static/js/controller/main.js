var gi = null


function configuracion_inicial(){
    webix.ajax().get('/values/admin', function(text, data){
        var values = data.json()
        show('cmd_ir_al_admin', values)
    })
    webix.ajax().get('/values/main', function(text, data){
        var values = data.json()
        $$('lbl_title_main').setValue(values.empresa)
        var pos = 4
        if(values.escuela){
            var node = {
                id: 'app_school',
                icon: 'graduation-cap',
                value: 'Escuela'}
            $$('main_sidebar').add(node, pos)
            pos += 1
        }
        if(values.nomina){
            var node = {
                id: 'app_nomina',
                icon: 'users',
                value: 'Nómina'}
            $$('main_sidebar').add(node, pos)
            pos += 1
        }
        if(values.punto_de_venta){
            var node = {
                id: 'app_tickets',
                icon: 'money',
                value: 'Punto de venta'}
            $$('main_sidebar').add(node, pos)
        }

        $$('cmd_update_timbres').define('badge', values.timbres)
        $$('cmd_update_timbres').refresh()
    })

}


function cmd_ir_al_admin_click(){
    window.location = '/admin'
}


var controllers = {
    init: function(){
        //~ Main
        $$('menu_user').attachEvent('onMenuItemClick', menu_user_click);
        configuracion_inicial()

        var tb_invoice = $$('tv_invoice').getTabbar()
        tb_invoice.attachEvent('onChange', tb_invoice_change)
        $$('prefilter_year').attachEvent('onChange', prefilter_year_change)
        $$('prefilter_month').attachEvent('onChange', prefilter_month_change)
        $$('cmd_delete_preinvoice').attachEvent('onItemClick', cmd_delete_preinvoice_click)
        $$('cmd_facturar_preinvoice').attachEvent('onItemClick', cmd_facturar_preinvoice_click)
        $$('cmd_update_timbres').attachEvent('onItemClick', cmd_update_timbres_click)
        $$('grid_preinvoices').attachEvent('onItemClick', grid_preinvoices_click)

        partners_controllers.init()
        products_controllers.init()
        bancos_controllers.init()
        invoices_controllers.init()
        controllers_school.init()
        nomina_controllers.init()
        tickets_controllers.init()
    }
}


function get_uso_cfdi_to_table(){
    webix.ajax().sync().get('/values/usocfdi', function(text, data){
        var values = data.json()
        table_usocfdi.clear()
        table_usocfdi.insert(values)
    })
}


function get_partners(){
    webix.ajax().get('/partners', {}, {
        error: function(text, data, xhr) {
            msg_error('Error al consultar')
        },
        success: function(text, data, xhr) {
            var values = data.json();
            $$('grid_partners').clearAll();
            if (values.data){
                $$('grid_partners').parse(values.data, 'json');
            };
        }
    })
}


function menu_user_click(id, e, node){
    if (id == 1){
        window.location = '/logout';
        return
    }
}


function current_dates(){
    var fy = $$('filter_year')
    var fm = $$('filter_month')
    var pfy = $$('prefilter_year')
    var pfm = $$('prefilter_month')
    var d = new Date()

    fy.blockEvent()
    fm.blockEvent()
    pfy.blockEvent()
    pfm.blockEvent()

    fm.setValue(d.getMonth() + 1)
    pfm.setValue(d.getMonth() + 1)
    webix.ajax().sync().get('/values/filteryears', function(text, data){
        var values = data.json()
        fy.getList().parse(values[0])
        pfy.getList().parse(values[1])
        fy.setValue(d.getFullYear())
        pfy.setValue(d.getFullYear())
    })

    fy.unblockEvent()
    fm.unblockEvent()
    pfy.unblockEvent()
    pfm.unblockEvent()
}


function multi_change(prevID, nextID){

    if(nextID == 'app_partners'){
        active = $$('multi_partners').getActiveId()
        if(active == 'partners_home'){
            get_partners()
        }
        return
    }

    if(nextID == 'app_products'){
        active = $$('multi_products').getActiveId()
        if(active == 'products_home'){
            get_products()
        }
        return
    }

    if(nextID == 'app_bancos'){
        active = $$('multi_bancos').getActiveId()
        if(active == 'banco_home'){
            get_cuentas_banco()
        }
        return
    }

    if(nextID == 'app_school'){
        active = $$('multi_school').getActiveId()
        if(active == 'school_home'){
            init_config_school()
        }
        return
    }

    if(nextID == 'app_nomina'){
        active = $$('multi_nomina').getActiveId()
        if(active == 'nomina_home'){
            default_config_nomina()
        }
        return
    }

    if(nextID == 'app_tickets'){
        active = $$('multi_tickets').getActiveId()
        if(active == 'tickets_home'){
            configuracion_inicial_ticket()
        }
        return
    }

    if(nextID == 'app_invoices'){
        active = $$('multi_invoices').getActiveId()
        if(active == 'invoices_home'){
            current_dates()
            get_invoices()
            validar_timbrar()
        }
        gi = $$('grid_invoices')
        return
    }

}


function get_taxes(){
    webix.ajax().sync().get('/values/taxes', function(text, data){
        var values = data.json()
        table_taxes.clear()
        table_taxes.insert(values)
        $$("grid_product_taxes").clearAll()
        $$("grid_product_taxes").parse(values, 'json')
    })
}


function cmd_update_timbres_click(){
    webix.ajax().get('/values/timbres', function(text, data){
        var value = data.json()
        $$('cmd_update_timbres').define('badge', value)
        $$('cmd_update_timbres').refresh()
    })
}