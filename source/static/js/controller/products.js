var cfg_products = new Object()


function products_default_config(){
    webix.ajax().get('/config', {'fields': 'productos'}, {
        error: function(text, data, xhr) {
            msg = 'Error al consultar'
            msg_error(msg)
        },
        success: function(text, data, xhr) {
            var values = data.json()
            cfg_products['inventario'] = values.chk_llevar_inventario
            cfg_products['predial'] = values.chk_config_cuenta_predial
            cfg_products['codigo_barras'] = values.chk_config_codigo_barras
            cfg_products['con_impuestos'] = values.chk_config_precio_con_impuestos
            cfg_products['default_unit'] = values.default_unidad
            cfg_products['default_tax'] = values.default_tax
            if(cfg_products['inventario']){
                $$('grid_products').showColumn('existencia')
            }
        }
    })
}


var products_controllers = {
    init: function(){
        $$('cmd_new_product').attachEvent('onItemClick', cmd_new_product_click)
        $$("cmd_edit_product").attachEvent("onItemClick", cmd_edit_product_click)
        $$("cmd_delete_product").attachEvent("onItemClick", cmd_delete_product_click)
        $$("cmd_save_product").attachEvent("onItemClick", cmd_save_product_click)
        $$("cmd_cancel_product").attachEvent("onItemClick", cmd_cancel_product_click)
        $$("cmd_import_products").attachEvent("onItemClick", cmd_import_products_click)
        $$("chk_automatica").attachEvent("onChange", chk_automatica_change)
        $$("valor_unitario").attachEvent("onChange", valor_unitario_change)
        $$('precio_con_impuestos').attachEvent('onChange', precio_con_impuestos_change)
        $$('precio_con_impuestos').attachEvent('onTimedKeyPress', precio_con_impuestos_key_up);
        $$("chk_inventario").attachEvent("onChange", chk_inventario_change)
        $$('grid_products').attachEvent('onItemDblClick', cmd_edit_product_click)
        products_default_config()
    }
}


function configurar_producto(){
    show('cuenta_predial', cfg_products['predial'])
    show('codigo_barras', cfg_products['codigo_barras'])
    show('precio_con_impuestos', cfg_products['con_impuestos'])
    show('chk_inventario', cfg_products['inventario'])
    show('txt_existencia', cfg_products['inventario'])
    show('txt_minimo', cfg_products['inventario'])
}


function get_categorias(){
    webix.ajax().sync().get('/values/categorias', function(text, data){
        var values = data.json()
        $$('categoria').getList().parse(values, 'plainjs')
    })
}


function get_products(){
    var grid = $$('grid_products')
    webix.ajax().get('/products', {}, {
        error: function(text, data, xhr) {
            msg_error('Error al consultar')
        },
        success: function(text, data, xhr) {
            var values = data.json();
            grid.clearAll();
            if (values.ok){
                grid.parse(values.rows, 'json');
                grid.refresh()
            };
        }
    });
}


function cmd_new_product_click(id, e, node){
    get_taxes()
    $$('unidad').getList().load('/values/unidades')
    configurar_producto()
    $$('form_product').setValues({
        id: 0, es_activo_producto: true})
    add_config({'key': 'id_product', 'value': ''})
    get_new_key()
    get_categorias()
    $$('unidad').setValue(cfg_products['default_unit'])
    $$('grid_product_taxes').select(cfg_products['default_tax'])
    $$('grid_products').clearSelection()
    $$("multi_products").setValue("product_new")
}


function cmd_edit_product_click(){
    get_taxes()
    $$('unidad').getList().load('/values/unidades')
    configurar_producto()
    var grid = $$('grid_products')
    var row = grid.getSelectedItem()
    if(row == undefined){
        msg_error('Selecciona un Producto')
        return
    }

    $$('categoria').getList().load('/values/categorias')
    webix.ajax().get('/products', {id:row['id']}, {
        error: function(text, data, xhr) {
            msg_error()
        },
        success: function(text, data, xhr){
            var values = data.json()
            $$('form_product').setValues(values.row)
            add_config({'key': 'id_product', 'value': values.row.id})
            for(i = 0; i < values.taxes.length; i++){
                $$('grid_product_taxes').select(values.taxes[i], true)
            }
        }
    })
    $$('multi_products').setValue('product_new')

};


function delete_product(id){
    webix.ajax().del('/products', {id:id}, function(text, xml, xhr){
        var msg = 'Producto eliminado correctamente'
        if(xhr.status == 200){
            $$('grid_products').remove(id)
            msg_ok(msg)
        }else{
            msg = 'No se pudo eliminar'
            msg_error(msg)
        }
    })
}


function cmd_delete_product_click(id, e, node){
    var row = $$('grid_products').getSelectedItem()
    if (row == undefined){
        msg_error('Selecciona un Producto')
        return
    }

    var msg = '¿Estás seguro de eliminar el Producto?<BR><BR>'
    msg += '(' + row['clave'] + ') ' + row['descripcion']
    msg += '<BR><BR>ESTA ACCIÓN NO SE PUEDE DESHACER<BR><BR>Se recomienda '
    msg += 'solo desactivar el producto en vez de eliminar'
    webix.confirm({
        title: 'Eliminar Producto',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if (result){
                delete_product(row['id'])
            }
        }
    })
}


function validate_sat_key_product(key, text){
    var result = false
    webix.ajax().sync().get('/values/satkey', {key:key}, function(text, data){
        result = data.json()
    })
    if(text){
        if(result.ok){
            return '<b>' + result.text + '</b>'
        }else{
            return '<b><font color="red">' + result.text + '</font></b>'
        }
    }
    return result.ok
}


function update_grid_products(values){
    var msg = 'Producto agregado correctamente'
    if(values.new){
        $$('form_product').clear()
        $$('grid_products').add(values.row)
    }else{
        msg = 'Producto actualizado correctamente'
        $$("grid_products").updateItem(values.row['id'], values.row)
    }
    $$('grid_products').refresh()
    $$('multi_products').setValue('products_home')
    msg_ok(msg)
}


function cmd_save_product_click(id, e, node){
    var msg = ''
    var form = this.getFormView()

    if(!form.validate()){
        msg_error('Valores inválidos')
        return
    }

    var rows = $$('grid_product_taxes').getSelectedId(true, true)
    if (rows.length == 0){
        msg_error('Selecciona un impuesto')
        return
    }

    var values = form.getValues();

    if(!validate_sat_key_product(values.clave_sat, false)){
        msg_error('La clave SAT no existe')
        return
    }

    if(values.descripcion.length > 1000){
        msg_error('Descripción con ' + values.descripcion.length + 'caracteres, captura solo 1000 caracteres')
        return
    }

    values['taxes'] = JSON.stringify(rows)
    webix.ajax().sync().post('products', values, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var values = data.json();
            if (values.ok) {
                update_grid_products(values)
            }else{
                msg_error(values.msg)
            }
        }
    })
}


function cmd_cancel_product_click(id, e, node){

    $$("multi_products").setValue("products_home")

};


function chk_automatica_change(new_value, old_value){
    var value = Boolean(new_value)
    if (value){
        var value = get_config('id_product')
        if(value){
            $$("clave").setValue(value)
            $$("clave").refresh()
        }else{
            get_new_key()
        }
        $$("clave").config.readonly = true
        $$('form_product').focus('clave_sat')
    } else {
        $$("clave").setValue('')
        $$("clave").config.readonly = false
        $$('form_product').focus('clave')
    }
    $$("clave").refresh()
}


function chk_inventario_change(new_value, old_value){
    var value = Boolean(new_value)
    if(value){
        $$('txt_existencia').enable()
        $$('txt_minimo').enable()
    }else{
        $$('txt_existencia').disable()
        $$('txt_minimo').disable()
        $$('txt_existencia').setValue(0)
        $$('txt_minimo').setValue(0)
    }
}


function get_new_key(){
    webix.ajax().get('/values/newkey', {
        error: function(text, data, xhr) {
            msg_error(text)
        },
        success: function(text, data, xhr) {
            var values = data.json();
            $$("clave").setValue(values.value)
            $$("clave").refresh()
        }
    })
}


function valor_unitario_change(new_value, old_value){
    if(!isFinite(new_value)){
        this.config.value = old_value
        this.refresh()
    }
}


function precio_con_impuestos_change(new_value, old_value){
    if(!isFinite(new_value)){
        this.config.value = old_value
        this.refresh()
    }
}


function calcular_sin_impuestos(value, taxes){
    var vu = $$('valor_unitario')
    var precio = value

    taxes.forEach(function(tax){
        var tasa = 1.00 + tax.tasa.to_float()
        if(tax.tipo == 'T' && tax.name == 'IVA'){
            precio = (value / tasa).round(DECIMALES)
        }
    })
    vu.setValue(precio)
}

function precio_con_impuestos_key_up(){
    var value = this.getValue()

    if(!value){
        return
    }

    var taxes = $$('grid_product_taxes').getSelectedItem(true)
    if (taxes.length == 0){
        msg = 'Selecciona al menos un impuesto'
        msg_error(msg)
        return
    }

    if(!isFinite(value)){
        msg = 'Captura un valor válido'
        msg_error(msg)
        return
    }
    calcular_sin_impuestos(parseFloat(value), taxes)
}


function cmd_import_products_click(){
    win_import_products.init()
    $$('win_import_products').show()
}


function cmd_upload_products_click(){
    var form = $$('form_upload_products')

    var values = form.getValues()

    if(!$$('lst_upload_products').count()){
        $$('win_import_products').close()
        return
    }

    if($$('lst_upload_products').count() > 1){
        msg = 'Selecciona solo un archivo'
        msg_error(msg)
        return
    }

    var template = $$('up_products').files.getItem($$('up_products').files.getFirstId())

    if(template.type.toLowerCase() != 'ods'){
        msg = 'Archivo inválido.\n\nSe requiere un archivo ODS'
        msg_error(msg)
        return
    }

    msg = '¿Estás seguro de importar este archivo?'
    webix.confirm({
        title: 'Importar Productos',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if(result){
                $$('up_products').send()
            }
        }
    })
}


function up_products_upload_complete(response){
    if(response.status != 'server'){
        msg = 'Ocurrio un error al subir el archivo'
        msg_error(msg)
        return
    }
    msg = 'Archivo subido correctamente.\n\nComenzando importación.'
    msg_ok(msg)
    $$('win_import_products').close()

    webix.ajax().post('/products', {opt: 'import'}, {
        error: function(text, data, xhr) {
            msg = 'Error al importar'
            msg_error(msg)
        },
        success: function(text, data, xhr) {
            var values = data.json();
            if (values.ok){
                get_products()
                webix.alert({
                    title: 'Importación terminada',
                    text: values.msg,
                })
            }else{
                msg_error(values.msg)
            }
        }
    })
}